import React from 'react'
import { Link } from 'react-router-dom'


const NoMatch = ({ location }) => (
  <div>
    <h3>No match for <code>{location.pathname}</code>. Return to <Link to='/'> Home </Link></h3>
  </div>
)

export default NoMatch