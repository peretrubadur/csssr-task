import React from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import ReactAutocomplete  from 'react-autocomplete'
import {
  fetchPostsWith,
  fetchReposWith
} from '../../modules/counter'

class Home extends React.Component {
  constructor (props) {
    super(props)
    this.state = { 
      value: '',
      items: [],
      notificationShown: false
    }
  }

  handleChange = (e) => this.setState({ value: e.target.value })

  fetchUserRepos = (username) => {
    this.props.fetchReposWith(username).then(() => {
      const {error, repos} = this.props.data
      if (Object.keys(error).length === 0 && error.constructor === Object) {
        this.setState({items: repos})
      }
      else {
        this.setState({
          notificationShown: true
        })
      }
    })
    // /users/:username/repos -- list of objects (property -- name)
  }

  fetchPosts = (url) => {
    this.props.fetchPostsWith(url).then(() => {
      const {error, repos} = this.props.data
      if (Object.keys(error).length === 0 && error.constructor === Object) {
        this.setState({items: repos})
      }
      else {
        this.setState({
          notificationShown: true
        })
      }
    })
    // /users/:username/repos -- list of objects (property -- name)
  }

  render = () => {
    const { inquiries } = this.props.data
    return (
      <div className="container is-fluid">
        {
          this.state.notificationShown ? (
              <div className="notification is-danger">
                <button className="delete" onClick={() => this.setState({
                  notificationShown: !this.state.notificationShown
                })}></button>
                <strong>{ this.props.data.error.status}: {this.props.data.error.message}</strong>
              </div>)
            : null
        }
        <div className="field has-addons">
          <div className="control">
            <div className="input">
              <ReactAutocomplete
                items={this.state.items}
                getItemValue={item => item.label}
                renderItem={(item, highlighted) =>
                  <div
                    key={item.id}
                    style={{ backgroundColor: highlighted ? '#eee' : 'transparent'}}
                  >
                    {item.label}
                  </div>
                }
                value={this.state.value}
                onChange={e => {
                  // после того, как стёрли значение, repos: []
                  this.setState({ 
                    value: e.target.value, 
                    items: []
                  })
                  if (e.target.value.substr(-1) === ' ') {
                    this.fetchUserRepos(e.target.value)
                  }
                }}
                onSelect={value => {
                  this.fetchPosts(this.state.value + value)
                  this.setState({
                    value: '', 
                    items: []
                  })

                }}
              />
            </div>
          </div>
          <div className="control">
            <a className={`button is-dark ${this.props.data.isRequesting ? 'is-loading' : ''}`}
              disabled={this.state.value === ''}
              onClick={() => {
                this.fetchPosts(this.state.value)
                this.setState({ 
                  value: '', 
                  items: []
                })
              }}>
              Search
            </a>
          </div>
        </div>

         <div className="container is-fluid">
          {
            inquiries &&
            inquiries.map((item, index) =>
              <div className="card" key={index}>
                <header className="card-header">
                  <p className="card-header-title">
                    {item.owner} / {item.repo}
                  </p>
                </header>
                <div className="card-content">
                  <div className="content">
                    Inquiry #{item.id}.
                    <br/>
                    <br/>
                    <time dateTime="2016-1-1">{item.date}</time>
                  </div>
                </div>
                <footer className="card-footer">
                      { item.issues && item.issues.length ? 
                      <Link className="card-footer-item" to={`/${item.id}`}>{item.issues.length} issues</Link>
                      : <div className="card-footer-item">No issues </div>}
                </footer>
              </div>
              )
          }

         </div>
      </div>
    )
  }
}


const mapStateToProps = state => ({
  data: state.data
})

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchPostsWith,
  fetchReposWith,
  changePage: (id) => push(`/${id}`)
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
