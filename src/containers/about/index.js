import React from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

const About = ( props ) => {
	const { inquiries } = props
	const { id } = props.match.params
	const inquiry = inquiries.length !== 0 && inquiries.find(elem => elem.id === parseInt(id, 10))
	if (inquiry) {
		return (
			<div className="container is-fluid">
				<div className="title is-1">{inquiry.owner}/{inquiry.repo}</div>
				<div className="container is-fluid">
					{
						inquiry.issues && inquiry.issues.map((issue, index) => 
							<div className="box" key={index}>
							  <article className="media">
							    <div className="media-left">
							      <figure className="image is-64x64">
							        <img src={issue.user.avatar_url} alt="profile pic" />
							      </figure>
							    </div>
							    <div className="media-content">
							      <div className="content">
							        <p>
							          <strong>{issue.title}</strong>{' '}<small><a href={issue.user.html_url} target="_blank">{issue.user.login}</a></small>
							          <br/>
							          { issue.body }
							        </p>
							      </div>
							    </div>
							  </article>
							</div>
						)
					}
				</div>

			</div>
		)
	}
	else {
		return (<h1> No data has been found for this inquiry. Return to <Link to='/'> Home </Link> </h1>)
	}
}

const mapStateToProps = state => ({
  inquiries: state.data.inquiries
})

const mapDispatchToProps = dispatch => bindActionCreators({
  changePage: (id) => push(`/${id}`)
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(About)