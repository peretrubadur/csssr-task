import React from 'react';
import { Route, Switch } from 'react-router-dom'
import Home from '../home'
import About from '../about'
import NoMatch from '../no-match'

const App = () => (
  <div>
    <main>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/:id" component={About}/>
        <Route path="*" component={NoMatch}/>
      </Switch>
    </main>
  </div>
)

export default App

// <Route exact path="/about-us" component={About} />