let nextId = 1
const GITHUB_API = 'https://api.github.com'

export const FETCH_REQUEST = 'FETCH_REQUEST'
export const FETCH_SUCCESS = 'FETCH_SUCCESS'
export const FETCH_ERROR = 'FETCH_ERROR'
export const FETCH_REPOS_SUCCESS = 'FETCH_REPOS_SUCCESS'
export const FETCH_REPOS_ERROR = 'FETCH_REPOS_ERROR'


const initialState = {
  count: 0,
  inquiries: [],
  repos: [],
  isRequesting: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    /* Fetching issues for particular project */
    case FETCH_REQUEST: 
      return {
        ...state,
        isRequesting: true
      }
    case FETCH_SUCCESS:
      return {
        ...state,
        isRequesting: !state.isRequesting,
        inquiries: [
          {
            id: nextId++,
            date: new Date().toLocaleString('en-US'),
            ...action.payload      
          },
          ...state.inquiries,
        ],
        repos: [],
        error: {}
      }
    case FETCH_ERROR:
      return {
        ...state,
        repos: [],
        isRequesting: !state.isRequesting,
        error: action.payload
      }

    /* Fetching repos for the user */

    case FETCH_REPOS_SUCCESS:
      return {
        ...state,
        isRequesting: !state.isRequesting,
        repos: action.payload,
        error: {}
      }
    case FETCH_REPOS_ERROR:
      return {
        ...state,
        repos: [],
        isRequesting: !state.isRequesting,
        error: action.payload
      }

    default:
      return state
  }
}

export const fetchPostsRequest = () => {
  return dispatch => {
    dispatch({
      type: 'FETCH_REQUEST'  
    })
  }
}

export const fetchPostsSuccess = (payload) => {
  return dispatch => {
    dispatch({
      type: 'FETCH_SUCCESS',
      payload
    })
  }
}

export const fetchPostsError = (payload) => {
  return dispatch => {
    dispatch({
      type: 'FETCH_ERROR',
      payload
    })
  }
}

export const fetchReposSuccess = (payload) => {
  return dispatch => {
    dispatch({
      type: 'FETCH_REPOS_SUCCESS',
      payload
    })
  }
}

export const fetchReposError = (payload) => {
  return dispatch => {
    dispatch({
      type: 'FETCH_REPOS_ERROR',
      payload
    })
  }
}



// ------------------------------------
// Helper functions
// ------------------------------------

export const fetchPostsWith = (url) => {
  return dispatch => {
    dispatch(fetchPostsRequest())
    const [ owner, repo ] = url.trim().split(' ')
    return fetchPosts(owner, repo).then(json => {
      const { status } = json[0]
      
      if (status === 200) {
        const posts = { issues: json[1], owner, repo }
        dispatch(fetchPostsSuccess(posts))
      }
      else {
        dispatch(fetchPostsError({
          status,
          message: json[1].message,
        }))
      }
    })
  }
}

export const fetchReposWith = (url) => {
  return dispatch => {
    dispatch(fetchPostsRequest())
    const username = url.trim()
    return fetchRepos(username).then(json => {
      const { status } = json[0]
      if (status === 200) {
        const repos = json[1].map(elem => {
          return {
          id: elem.name,
          label: elem.name
          }
        })
        dispatch(fetchReposSuccess(repos))
      }
      else {
        dispatch(fetchReposError({
          status,
          message: json[1].message,
        }))
      }
    })
  }
}

function fetchRepos (username) {
  const URL = `${GITHUB_API}/users/${username}/repos`
  return fetch(URL).then(response => Promise.all([response, response.json()]))
}

function fetchPosts (owner, repo) {
  const URL = `${GITHUB_API}/repos/${owner}/${repo}/issues`
  return fetch(URL).then(response => Promise.all([response, response.json()]))
}