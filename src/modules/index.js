import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import data from './counter'

export default combineReducers({
  router: routerReducer,
  data
})
